var Ajv = require('C:/Users/Viktor/node_modules/ajv');
//var set = require('C:/Users/Viktor/node_modules/lodash/set');
var ajv = new Ajv({allErrors: true});
require('C:/Users/Viktor/node_modules/ajv-keywords')(ajv, 'switch');

const reWeburl =
    "^" +
    // protocol identifier
    "(?:(?:https?|ftp)://)" +
    // user:pass authentication
    "(?:\\S+(?::\\S*)?@)?" +
    "(?:" +
    // IP address exclusion
    // private & local networks
    "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
    "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
    "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
    // IP address dotted notation octets
    // excludes loopback network 0.0.0.0
    // excludes reserved space >= 224.0.0.0
    // excludes network & broacast addresses
    // (first & last IP address of each class)
    "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
    "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
    "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
    "|" +
    // host name
    "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
    // domain name
    "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
    // TLD identifier
    "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
    // TLD may end with dot
    "\\.?" +
    ")" +
    // port number
    "(?::\\d{2,5})?" +
    // resource path
    "(?:[/?#]\\S*)?" +
    "$";

const checkUrl = {
    anyOf: [
        {type: 'null'},
        {allOf: [{type: 'string'}, {pattern: reWeburl}]}
    ]
};

var schema = {
    switch: [
        {
            if: {properties: {ad_type: {const: 1}}},
            then: {required: ['width', 'height']}
        }
    ],
    required: ['name', 'bcpm', 'modality', 'video_type'],
    properties: {
        name: {type: 'string', minLength: 1, maxLength: 255},
        campaign_id: {type: 'integer'},
        modality: {type: 'integer', minimum: 0, maximum: 2},
        handle_https: {type: 'boolean'},
        frequency_capping: {
            anyOf: [
                {type: 'null'},
                {allOf: [{type: 'integer'}, {minimum: 0}]}]
        },
        duration: {const: 0},
        daily_capping: {
            anyOf: [
                {type: 'null'},
                {allOf: [{type: 'integer'}, {minimum: 0}]}]
        },
        analytics: {anyOf: [{const: 0}, {const: 1}]},
        ad_type: {anyOf: [{const: 1}, {const: 2}]},
        ad_serving: {anyOf: [{const: 1}, {const: 2}]},
        bcpm: {type: 'number', minimum: 0},
        video_type: {anyOf: [{const: 1}, {const: 2}]},
        width: {type: 'integer'},
        height: {type: 'integer'},
        weight: {type: 'integer', minimum: 0, maximum: 10},
        target: {const: '_self'},
        html_template: {anyOf: [{type: 'null'}, {type: 'string'}]},
        click_tracking_url: checkUrl,
        impressions_tracking_url: checkUrl,
        error_tracking_url: checkUrl
    }
};

var data = {
    name: "Bob",
    campaign_id: 700,
    modality: 1,
    handle_https: true,
    frequency_capping: null,
    duration: 0,
    daily_capping: null,
    analytics: 1,
    ad_type: 1,
    ad_serving: 1,
    bcpm: 1,
    video_type: 1,
    width: 480,
    height: 60,
    weight: 2,
    target: '_self',
    html_template: null,
    click_tracking_url: 'https://www.ukr.net/ua/',
    impressions_tracking_url: null,
    error_tracking_url: 'https://www.google.com.ua/'
};

var valid = ajv.validate(schema, data);
if (!valid) {
    const errors = ajv.errors;
    console.log(errors);
} else {
    console.log('Valid !');
}
